import React, { useState, useEffect } from "react";

function ShoeList(props) {

  const [shoes, setShoes] = useState([])

  async function fetchShoes() {
    const response = await fetch('http://localhost:8080/api/shoes/');
  
    if (response.ok) {
      const data = await response.json()
      console.log(data)
      setShoes(data.shoes)
    }
}


  useEffect(() => {
    fetchShoes();
  }, []);

  const handleDelete = async (id) => {
    const deleteUrl = `http://localhost:8080/api/shoes/${id}`
    const fetchConfig = {
      method: "delete",
    }

    const response = await fetch(deleteUrl, fetchConfig)
    if (response.ok) {
      fetchShoes();
    } else {
      console.error(response)
    }
  }

    return(
      <table className="table table-striped">
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Model Name</th>
          <th>Color</th>
          <th>Picture Url</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map(shoe => {
          return (
            <tr key={shoe.href}>
              <td>{ shoe.manufacturer }</td>
              <td>{ shoe.model_name }</td>
              <td>{ shoe.color }</td>
              <td scope="row"><img src={shoe.picture_url} alt={`Image of ${shoe.model_name} ${shoe.manufacturer} ${shoe.color}`} className="img-thumbnail" style={{ maxWidth: '10em', maxHeight: '10em' }}/></td>
              <td>
                <button onClick={() => handleDelete(shoe.id)} className="btn btn-danger">Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
    );
  }

  export default ShoeList;
