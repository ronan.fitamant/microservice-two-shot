import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import ShoeList from './ShoeList';
import HatForm from './HatForm';
import ShoeForm from './ShoeForm';

function App(props) {

  return (
    <>
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='shoes'>
            <Route index element={<ShoeList shoes={props.shoes} />} />
            <Route path="new" element={<ShoeForm />}></Route>
          </Route>
          <Route path='hats'>
            <Route index element={<HatList hats={props.hats} />} />
            <Route path='new' element={<HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
    </>
  );
}

export default App;
