import React, {useState, useEffect} from 'react';

function ShoeForm () {


    const [manufacturer, setManufacturer] = useState('')

    const handleManufacturerNameChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const [modelName, setModelName] = useState('')

    const handleModelNameChange = (event) => {
        const value = event.target.value
        setModelName(value)
    }

    const [color, setColor] = useState('')

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const [pictureUrl, setPictureUrl] = useState('')

    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }

    const [bin, setBin] = useState('')

    const handleBinChange =(event) => {
        const value = event.target.value
        setBin(value)
    }

    const [bins, setBins] = useState([])


    const getData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        setBins(data.bins);
        }
    }

    useEffect(()=> {
        getData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();
      
        const data = {};

        data.manufacturer = manufacturer;
        data.model_name = modelName
        data.color = color
        data.picture_url = pictureUrl
        data.bin = bin
        console.log(data);
      
        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
      
    const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
          const newShoe = await response.json();
          console.log(newShoe);

          setManufacturer('');
          setModelName('');
          setColor('');
          setPictureUrl('');
          setBin('');

        }
      }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-a-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleManufacturerNameChange} value={manufacturer} placeholder="Manufacturer" id="manufacturer" required name="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleModelNameChange} value={modelName} placeholder="Model Name" required name="model_name" type="text" id="model_name" className="form-control" />
              <label htmlFor="model_name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} value={color} placeholder="Color" type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="Picture Url" required name="picture_url" type="url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture Url</label>
            </div>
            <div className="mb-3">
              <select onChange={handleBinChange} value={bin} required name="bin" className="form-select" id="bin">
                <option value="">Choose a bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.href} value={bin.href}>
                        {bin.closet_name}
                        </option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;
