import { useState, useEffect } from "react";

function HatList(props) {
    const [hats, setHats] = useState([]);

    async function fetchHats() {
        const response = await fetch('http://localhost:8090/api/hats/');

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    }


    useEffect(() => {
        fetchHats();
    }, [])

    async function deleteHat(id) {
        const response = await fetch(`http://localhost:8090/api/hats/${id}`, {
            method: 'DELETE'
        });
        if (response.ok) {
            fetchHats();
        }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style Name</th>
                    <th>Color</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.href}>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.style_name }</td>
                            <td>{ hat.color }</td>
                            <td scope="row"><img src={hat.picture_url} alt={`Image of ${hat.fabric} ${hat.style_name} ${hat.color}`} className="img-thumbnail" style={{ maxWidth: '10em', maxHeight: '10em' }}/></td>
                            <td>
                                <button onClick={() => deleteHat(hat.id)} className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
};

export default HatList;
