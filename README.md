# Wardrobify

Team:

* Erik Kordylasinski - Shoes
* Ronan Fitamant - Hats

## Design
Bootstrap

## Shoes microservice

The code allows the wardrobe microservice to integrate with the wardrobe API, retrieve the bin data and keep the local database updated with the bins in the wardrobe microservice.

## Hats microservice

In our Django/React website, we have this storage unit called the wardrobe microservice where we keep all our hats. The website talks to the storage unit using these special messages called "HTTP requests" to do stuff like adding new hats, checking what hats we have, or even getting rid of hats. We also have this smart messaging system called "poll" that helps the hat and wardrobe talk to each other, making sure our hat collection is always up to date on the website.
