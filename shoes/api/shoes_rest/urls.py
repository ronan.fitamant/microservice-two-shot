from django.urls import path
from .views import api_list_shoes, api_shoe

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_shoes"),
    path("wardrobes/<int:bin_vo_id>/shoes", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:pk>/",  api_shoe, name="api_shoe"),
   
    
]
